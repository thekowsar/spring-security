package com.kowsar.springsecurity.constants;

public interface EndpointConstant {

    String BASE_URL =  "security";

    String GREET_URL = BASE_URL + "/greet";
    String HOME_URL = "/";
    String USER_URL = BASE_URL + "/user";
    String ADMIN_URL = BASE_URL + "/admin";

}
