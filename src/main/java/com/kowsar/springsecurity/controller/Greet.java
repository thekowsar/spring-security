package com.kowsar.springsecurity.controller;

import com.kowsar.springsecurity.constants.EndpointConstant;
import com.kowsar.springsecurity.dto.ResponseDto;
import com.kowsar.springsecurity.dto.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.kowsar.springsecurity.constants.EndpointConstant.*;

@Slf4j
@RestController
public class Greet {

    @GetMapping(value = GREET_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> greet(@RequestParam("name") String name) {
        log.info("Request received {} for {}", GREET_URL, name);
        ResponseEntity response = new ResponseEntity<>(
                ResponseDto.builder()
                        .responseStatus(ResponseStatus.OK)
                        .responseMessage("Success")
                        .data(name.concat(", how are you?"))
                        .build(),
                HttpStatus.OK
        );
        log.info("Response returned {} for {}", GREET_URL, response);
        return response;
    }

    @GetMapping(value = HOME_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> home(){
        log.info("Request received {}", HOME_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseStatus(ResponseStatus.OK)
                        .responseMessage("SUCCESS")
                        .data("WELCOME HOME")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} fro {}", HOME_URL, response);
        return response;
    }

    @GetMapping(value = USER_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> user(){
        log.info("Request received {}", USER_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseStatus(ResponseStatus.OK)
                        .responseMessage("SUCCESS")
                        .data("WELCOME USER")
                        .build() ,
                HttpStatus.OK
        );
        log.info("Response returned {} for {}", USER_URL, response);
        return response;
    }

    @GetMapping(value = ADMIN_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> admin(){
        log.info("Request received {}", ADMIN_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseStatus(ResponseStatus.OK)
                        .responseMessage("SUCCESS")
                        .data("WELCOME ADMIN")
                        .build() ,
                HttpStatus.OK
        );
        log.info("Response received {} for {}", ADMIN_URL, response);
        return response;
    }

}
