package com.kowsar.springsecurity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto<T> implements Serializable {

    private ResponseStatus responseStatus;

    private String responseMessage;

    private T data;
}
