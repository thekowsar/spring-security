package com.kowsar.springsecurity.dto;

public enum ResponseStatus {

    OK,
    FAILED,
    CREATED,
    UPDATED,
    DELETED,
    NOT_FOUND

}
